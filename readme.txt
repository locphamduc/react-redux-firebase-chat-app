PHẦN MỀM CHAT REALTIME SỬ DỤNG REACT VÀ FIREBASE
********************************************************************************************************************************
CÁC CHỨC NĂNG ĐÃ THỰC HIỆN:
- Login bằng google authen popup (2 điểm)
- Hiển thị danh sách user trong database (1 điểm, chưa sort những user chat gần nhất, chưa hiển thị user online và last login time)
- Chat với 1 trong các user trong database (4 điểm)
- Upload lên firebase hosting (2 điểm)

********************************************************************************************************************************
HƯỚNG DẪN SỬ DỤNG:
- cd đến thư mục Source.
- chạy lệnh npm install để cài các package cần thiết.
- chạy lệnh npm start để thực hiện test app tại local (http://localhost:3000).
- chạy lệnh npm run build để tạo folder build phục vụ nhu cầu deploy lên firebase 
- thao tác deploy project lên firebase tìm hiểu thiêm tại link: "https://www.youtube.com/watch?v=jFtdF_ECA10&t=438s"
-----------------------------------------------------------------------------------------------------------------------------------------
static firebase hosting đã được tạo tại: "https://react-firebase-chat-app-16cf9.firebaseapp.com/"
để sử dụng ứng dụng chat trước hết cần bấm đăng nhập. sẽ có popup của google hiện lên yêu cầu đăng nhập.
Sau khi đăng nhập sẽ hiển thị danh sách user có thể chat nằm ở phía bên trái màn hình.
Chọn một user trong số các user để hiển thị giao diện khung chat và chat với đối phương.
**** Khuyến nghị sử dụng 2 acc clone để test vì hiện tại ứng dụng chưa có notify để thông báo tin nhắn mới ****
