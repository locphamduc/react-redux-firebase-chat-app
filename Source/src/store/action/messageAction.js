import { createMessageKey } from '../../common/util'

export const setReceiver = (receiverId) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const receiverData = getState().firestore.data.user[receiverId]
    const receiver = {
      id: receiverId,
      userName: receiverData.userName,
      userAvatar: receiverData.userAvatar
    }    

    const userId = getState().auth.user.id
    const key = createMessageKey(userId, receiverId)

    dispatch({ type: 'SET_CURRENT_RECEIVER', data: {
      receiver: receiver,
      key: key
    }})

    const firestore = getFirestore()
    firestore.set({ collection: 'message',
                    doc: key
                  }, 
                  {
                    key: key
                  }
                )       
  }
}

export const saveMessage = (userId1, userId2, message) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    console.log('saveMessage')

    const firestore = getFirestore()
    const key = getState().message.messageKey
    const currentTime = (Date.now()).toString()
    firestore.set({ collection: 'message',
                    doc: key, 
                    subcollections: [{ collection: 'messages', doc: currentTime}]
                  },
                  {
                    sender: userId1,
                    content: message
                  }
                )
  }
}