import { provider } from '../../config/firebase-config'

export const login = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase()

    firebase.auth().signInWithPopup(provider)
    .then((data) => {
      const user = {
        id: data.user.uid,
        userName: data.user.displayName,
        userAvatar: data.user.photoURL
      }

      const firestore = getFirestore()
      firestore.set({ 
          collection: 'user',
          doc: data.user.uid,
        }, {
          userName: data.user.displayName,
          userAvatar: data.user.photoURL
        }
      )
      dispatch({ type: 'LOGIN_SUCCESS', data: user})
    })
    .catch((err) => {
      dispatch({ type: 'LOGIN_ERROR', data: err})
    })
  }
}

export const logout = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase()
    
    firebase.auth().signOut().then(() => {
      dispatch({ type: 'SIGNOUT_SUCCESS'})
    })
  }
}

export const checkUserLogin = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase()

    firebase.auth().onAuthStateChanged(data => {
      if (data) {
        const user = {
          id: data.uid,
          userName: data.displayName,
          userAvatar: data.photoURL
        }
        dispatch({ type: 'LOGIN_SUCCESS', data: user})
      }
    })
  }
}