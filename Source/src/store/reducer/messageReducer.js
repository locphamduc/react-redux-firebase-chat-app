const initState = {
  messageKey: null,
  currentReceiver: null,
}

const messageReducer = (state = initState, action) => {
  switch(action.type) {
    case 'SET_CURRENT_RECEIVER':
      console.log('set current receiver')
      return {
        ...state,
        currentReceiver: action.data.receiver,
        messageKey: action.data.key,
      }
    case 'SIGNOUT_SUCCESS':
      return {
        ...state,
        currentReceiver: null,
        messageKey: null,
      }
    default:
      return state
  }
}

export default messageReducer