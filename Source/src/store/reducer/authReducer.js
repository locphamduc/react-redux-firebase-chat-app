const initState = {
  user: null,
  authError: null
}

const authReducer = (state = initState, action) => {
  switch(action.type) {
    case 'LOGIN_ERROR':
      console.log('login error')
      return {
        ...state,
        authError: action.data,
        user: null
      }  
    case 'LOGIN_SUCCESS':
      console.log('login success')
      return {
        ...state,
        authError: null,
        user: action.data
      }
    case 'SIGNOUT_SUCCESS':
      console.log('sigout success')
      return {
        ...state,
        authError: null,
        user: null,
      }
    default:
      return state
  }
}

export default authReducer