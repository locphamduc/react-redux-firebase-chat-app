import React, { Component } from 'react'
import User from '../component/User'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux';
import { setReceiver } from '../store/action/messageAction'

class SideBar extends Component {
  state = {
    searchText: '',
    listUser: []
  }

  handleSearch = (e) => {
    // console.log(this.state.searchText)
    if (e.charCode === 13) {
      let searchText = this.state.searchText
      if (searchText === '') {
        this.setState({
          listUser: this.props.listUser
        })
      } else {
        let listUser = this.props.listUser.filter(user => {
          console.log(user.userName.toUpperCase().indexOf(searchText) > -1 )
          return user.userName.toUpperCase().indexOf(searchText) > -1 
        })
        this.setState({
          listUser: listUser
        })
      }
    }
  }

  render() {
    let listUserRender = null
    if (this.state.listUser && this.props.listUser && this.props.user) { 
      const listUser = (this.state.listUser.length !== 0) ? (
        this.state.listUser.filter(user => {
        return user.id !== this.props.user.id
      })) : (
        this.props.listUser.filter(user => {
        return user.id !== this.props.user.id
      }))

      listUserRender = listUser.map(user => {
        return ( 
          <User userId={user.id} 
                userName={user.userName} 
                userAvatar={user.userAvatar} 
                key={user.id}
                setReceiver={this.props.setReceiver}/>
        )
      })
    }
  
    return(
      <div className="side-bar">
        <input type="text" 
               className="form-control search-input"
               onKeyPress={this.handleSearch}
               onChange={e => this.setState({ searchText: e.target.value.toUpperCase()})}></input>
        {listUserRender}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    listUser: state.firestore.ordered.user,
    user: state.auth.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setReceiver: (uid) => dispatch(setReceiver(uid)),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([{ collection: 'user'}])
)(SideBar)