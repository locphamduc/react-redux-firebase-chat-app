import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login, logout, checkUserLogin } from '../store/action/authAction'

class NavBar extends Component {

  handleLogin = () => {
    this.props.login()
  }

  handleLogout = () => {
    this.props.logout()
  }

  componentDidMount = () => {
    this.props.checkUserLogin()
  }

  render() {
    let navBarData = 0
    
    if (this.props.user) {
      let userName = this.props.user.userName
      let userAvatar = this.props.user.userAvatar
      
      navBarData = (<div className="container">
                      <div className="col-md-3">
                        <img src={userAvatar} className="profile-image rounded-circle" alt="userAvatar"/> 
                        <a className="navbar-brand profile-name" href="/">{userName}</a>
                      </div>
                      <button className="btn btn-outline-secondary my-2 my-sm-0" onClick={this.handleLogout}>Đăng xuất</button> 
                    </div>) 
    } 
    else {
      navBarData = (<div className="container">
                      <a className="navbar-brand" href="/">Vui lòng đăng nhập để sử dụng!!</a>
                      <button className="btn btn-outline-primary my-2 my-sm-0" onClick={this.handleLogin}>Đăng nhập</button>
                    </div>)
    }
    return (
      <div>
        <nav className="navbar navbar-light bg-light justify-content-between">
          {navBarData}
        </nav>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: () => dispatch(login()),
    logout: () => dispatch(logout()),
    checkUserLogin: () => dispatch(checkUserLogin()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar) 