import React, { Component } from 'react';
import ListMessage from '../component/ListMessage';
import { compose } from 'redux';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { saveMessage } from '../store/action/messageAction'
import { scrollToBottomMessages, 
         convertObjectMessageListToArrayMessageList } from '../common/util'

const noUser = require('../public/image/user.png');

class ChatArea extends Component {
  state = {
    message: "",
  }

  handleSubmit = (e) => {
    if (e.charCode === 13 && this.state.message.trim() !== "" && this.props.user1 && this.props.user2) {
      console.log('handleSubmit')
      this.props.saveMessage(this.props.user1.id, this.props.user2.id ,this.state.message)
      this.setState({ message: "" })
    }
  }


  componentDidUpdate = () => {
    scrollToBottomMessages();
  }

  render() {
    const currentChat = (this.props.user2 !== null) ? (
      <div className="current-chat" >
        <img src={this.props.user2.userAvatar} className="profile-image rounded-circle" alt="userAvatar"/> 
        <p className="navbar-brand profile-name">{this.props.user2.userName}</p>
      </div>
    ) : (
      <div className="current-chat" >
        <img src={noUser} className="profile-image rounded-circle" alt="UserAvatar"/> 
        <p className="navbar-brand profile-name">No friend selected</p>
      </div>
    )

    const listMessage = (this.props.user1 !== null && this.props.user2 !== null) ? (
      <ListMessage userId1={this.props.user1.id} userId2={this.props.user2.id} listMessages={this.props.listMessages}/>
    ) : null

    return(
      <div>
        <div className="message-board">
          {currentChat}
          <div className="message-list" id="message-list">
            {listMessage}
          </div>
        </div>
        <div className="row">
          <div className="col-md-10">
            <div className="message-input">
              <input type="text" 
                    className="form-control" 
                    value={this.state.message} 
                    onKeyPress={this.handleSubmit}
                    onChange={e => this.setState({ message: e.target.value })}
                    placeholder="Type and enter to send message..."/>
            </div>
          </div>
          <div className="file btn btn-primary">
            Upload
            <input type="file" className="file"/>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let listMessage = null
  
  if (state.message.messageKey && 
    state.firestore.data.message &&
    state.firestore.data.message[state.message.messageKey]) {
    let messages = state.firestore.data.message[state.message.messageKey].messages
    listMessage = convertObjectMessageListToArrayMessageList(messages)
  }

  return {
    user1: state.auth.user,
    user2: state.message.currentReceiver,
    messageKey: state.message.messageKey,
    listMessages: listMessage
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveMessage: (userId1, userId2, message) => dispatch(saveMessage(userId1, userId2, message))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect((props) => [`/message/${props.messageKey}/messages`]
  )
)(ChatArea);