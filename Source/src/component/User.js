import React from 'react'

const User = ({userId, userName, userAvatar, setReceiver}) => {
  return (
    <div className="member" id={userId} onClick={() => setReceiver(userId)}>
      <img src={userAvatar} alt=""/>
      <p className="user-name">{userName}</p>
      <span className="user-subhead">Member</span>
    </div>
  )
}

export default User;