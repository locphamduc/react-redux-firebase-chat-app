import React from 'react'
import dayjs from 'dayjs'
import { isImageUrl } from '../common/util'

const ListMessage = ({userId1, userId2, listMessages}) => {  
  let List = null
  if (listMessages !== null) {
    List = listMessages.map(message => {
      let messageContent = null
      if (isImageUrl(message.content)) {
        messageContent = <img src={message.content} alt="chat-img" className="chat-img" />
      } else {
        messageContent = <p>{message.content}</p>
      }

      if (message.sender === userId1) {
        return (
          <div className="outgoing_msg" key={message.time}>
            <div className="sent_msg">
              {messageContent}
              <span className="time_date">{dayjs(message.time).format("DD-MM-YYYY HH:mm ")}</span> </div>
          </div>
        )
      }
      else {
        return (
          <div className="incoming_msg" key={message.time}>
            <div className="received_msg">
              {messageContent}
              <span className="time_date">{dayjs(message.time).format("DD-MM-YYYY HH:mm ")}</span>
            </div>
          </div>
        )
      }
    })
  }

  return (
    <div>
      <div className="messaging">
        <div className="mesgs">
          <div className="msg_history">
            {List}
          </div>
        </div>
      </div>
  </div>
  )
}

export default ListMessage;