import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var config = {
  apiKey: "AIzaSyCWvJWRnKTVNJP2zl4zqzjnF2HhWkcbwhI",
  authDomain: "react-firebase-chat-app-16cf9.firebaseapp.com",
  databaseURL: "https://react-firebase-chat-app-16cf9.firebaseio.com",
  projectId: "react-firebase-chat-app-16cf9",
  storageBucket: "react-firebase-chat-app-16cf9.appspot.com",
  messagingSenderId: "1012340708294"
};

firebase.initializeApp(config)
firebase.firestore().settings({ timestampsInSnapshots: true });

export const provider = new firebase.auth.GoogleAuthProvider();
export default firebase;