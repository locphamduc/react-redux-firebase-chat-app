import React, { Component } from 'react';
import NavBar from './container/NavBar'
import Sidebar from './container/SideBar'
import ChatArea from './container/ChatArea'
import { connect } from 'react-redux'

import './App.css';

class App extends Component {
  
  render() {
    let sideBar = null
    let chatArea = null
    if (this.props.user) {
      sideBar = <Sidebar />
    }
    if (this.props.currentReceiver) {
      chatArea = <ChatArea />
    }

    return (
      <div>
        <NavBar />
        <div className="wrapper">
          <div className="container" id="body-container">
            <div className="row">
              <div className="col-md-3">
                {sideBar}
              </div>
              <div className="col-md-9">
                {chatArea}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    currentReceiver: state.message.currentReceiver,
  }
}

export default connect(mapStateToProps)(App);
