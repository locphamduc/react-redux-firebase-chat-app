export const createMessageKey = (userId1, userId2) => {
  if (userId1 < userId2) {
    return userId1 + userId2
  } 
  return userId2 + userId1
}

export const scrollToBottomMessages = () => {
  var objDiv = document.getElementById("message-list");
  objDiv.scrollTop = objDiv.scrollHeight;
}

export const convertObjectUserListToArrayUserList = (users) => {
  let listUser = []
  for(var propt in users){
    listUser.push({
      id: parseInt(propt),
      userName: users[propt].userName,
      userAvatar: users[propt].userAvatar,
    })
  }
}

export const convertObjectMessageListToArrayMessageList = (messages) => {
  let listMessage = []
  for(var propt in messages){
    listMessage.push({
      time: parseInt(propt),
      sender: messages[propt].sender,
      content: messages[propt].content,
    })
  }
  return listMessage
}

export const isImageUrl = (url) => {
  return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
}